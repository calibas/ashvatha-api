<?php

namespace Drupal\ashvatha_api\Plugin\rest\resource;

use Drupal\Core\Entity\EntityInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\group\Entity;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\node\Entity\Node;
use Drupal\group\GroupMembership;
use Drupal\image\Entity\ImageStyle;

/**
 * Provides a resource to get and patch asset type terms
 *
 * @RestResource(
 *   id = "group_add_node_resource",
 *   label = @Translation("Group Add Node Resource"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/api/groupadd/disabled",
 *     "https://www.drupal.org/link-relations/create" = "/api/groupadd"
 *   }
 * )
 */
class GroupAddNodeResource extends ResourceBase
{

    /**
     * Responds to GET requests.
     *
     * @return ResourceResponse
     */
    public function get()
    {
        $response = ['message' => 'Get disabled'];
        return new ResourceResponse($response);
    }

     /**
     * Responds to POST requests.
     *
     * @param array $data
     *   An array with the payload.
     *
     * @return ResourceResponse
     */
    public function post(array $data)
    {
        //\Drupal::logger('ashvatha_api')->notice('GroupID:' . $data['group-id']);
        //\Drupal::logger('ashvatha_api')->notice('<pre><code>' . print_r($data, TRUE) . '</code></pre>');

        $node = Node::load($data['node-id']);
        $plugin_id = 'group_node:' . $node->bundle();
        $group = Group::load($data['group-id']);
        $group->addContent($node,$plugin_id);

        $response = ['message' => 'Success'];
        return new ResourceResponse($response);
    }
}